<?php

return [
	'log' => [
		'enabled' => env('GLONASSGPS_LOG_ENABLED', false),
	],
];
