<?php

namespace GlonassGPS;

use Illuminate\Support\ServiceProvider;

class GlonassGPSServiceProvider extends ServiceProvider
{
	public function register()
	{
		$this->mergeConfigFrom(__DIR__.'/../config/glonass-gps.php', 'glonass-gps');
	}

	public function boot()
	{
		if ($this->app->runningInConsole()) {

			$this->publishes([
				__DIR__.'/../config/glonass-gps.php' => config_path('glonass-gps.php'),
			], 'config');

		}
	}
}
