<?php

namespace GlonassGPS;

use Carbon\Carbon;
use GlonassGPS\Exceptions\GlonassGPSException;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

/**
 * Class Client
 * @package GlonassGPS
 */
class Client
{
	/** @var string $url */
	private $url;

	/** @var string $token */
	private $token;

	public function __construct(string $url, string $token)
	{
		$this->url = $url;
		$this->token = $token;
	}

	/**
	 * Authenticate
	 * Аутентификация в сервисе через логин-пароль
	 *
	 * @param string $url
	 * @param string $login
	 * @param string $password
	 * @return string
	 * @throws GlonassGPSException
	 */
	public static function authenticate(string $url, string $login, string $password): string
	{
		$guzzle = new GuzzleClient();
		$fullURL = "$url/ServiceJSON/Login";
		$query = [
			'UserName' => $login,
			'Password' => $password,
		];

		if (config('glonass-gps.log.enabled')) {
			$fileName = Carbon::now()->format('Y-m-d-H:i:s') . '-' . 'login';
			file_put_contents(storage_path('logs/glonass-gps') . "/$fileName-request.log", json_encode([
				'url' => $fullURL,
				'query' => $query,
			]));
		}

		try {
			$response = $guzzle
				->get($fullURL, [
					'query' => $query,
				])
				->getBody()
				->getContents();

			return $response;
		} catch (ServerException | ClientException $exception) {
			if (config('glonass-gps.log.enabled')) {
				try {
					$data = $exception->getResponse()->getBody()->getContents();
					file_put_contents(storage_path('logs/glonass-gps') . "/$fileName-response-error.log", $data);
				} catch (\Exception $exception) {}
			}

			throw new GlonassGPSException("Ошибка аутентификации на сервере {$url}");
		}
	}

	public function getEnumSchemas()
	{
		return $this->executeGet('/ServiceJSON/EnumSchemas');
	}

	public function getEnumDevices($schemaID)
	{
		return $this->executeGet('/ServiceJSON/EnumDevices', [
			'schemaID' => $schemaID,
		]);
	}

	public function getLocation($schemaID, $carID)
	{
		return $this->executeGet('/ServiceJSON/GetOnlineInfo', [
			'schemaID' => $schemaID,
			'IDs' => $carID,
			'session' => $this->token,
		]);
	}

	private function executeGet(string $endpoint, array $query = [])
	{
		$guzzle = new GuzzleClient();
		$query = array_merge($query, [
			'session' => $this->token,
		]);

		if (config('glonass-gps.log.enabled')) {
			$fileName = Carbon::now()->format('Y-m-d-H:i:s');
			file_put_contents(storage_path('logs/glonass-gps') . "/$fileName-request.log", $query);
		}

		try {
			$response = $guzzle
				->get("{$this->url}{$endpoint}", [
					'query' => $query,
				])
				->getBody()
				->getContents();

			$json = json_decode($response, true);

			if (!is_null($json)) {
				if (config('glonass-gps.log.enabled')) {
					file_put_contents(storage_path('logs/glonass-gps') . "/$fileName-response.log", $json);
				}
				return $json;
			}

			if (config('glonass-gps.log.enabled')) {
				file_put_contents(storage_path('logs/glonass-gps') . "/$fileName-response.log", $response);
			}
			return $response;
		} catch (ClientException | ServerException $exception) {
			if (config('glonass-gps.log.enabled')) {
				try {
					$data = $exception->getResponse()->getBody()->getContents();
					file_put_contents(storage_path('logs/glonass-gps') . "/$fileName-response-error.log", $data);
				} catch (\Exception $exception) {}
			}
			throw new GlonassGPSException('Ошибка выполнения запроса');
		}
	}
}
